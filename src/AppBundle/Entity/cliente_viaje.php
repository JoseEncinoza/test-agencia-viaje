<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * cliente_viaje
 *
 * @ORM\Table(name="cliente_viaje")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\cliente_viajeRepository")
 */
class cliente_viaje
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_cliente", type="integer")
     */
    private $idCliente;

    /**
     * @var int
     *
     * @ORM\Column(name="id_viaje", type="integer")
     */
    private $idViaje;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCliente
     *
     * @param integer $idCliente
     *
     * @return cliente_viaje
     */
    public function setIdCliente($idCliente)
    {
        $this->idCliente = $idCliente;

        return $this;
    }

    /**
     * Get idCliente
     *
     * @return int
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    /**
     * Set idViaje
     *
     * @param integer $idViaje
     *
     * @return cliente_viaje
     */
    public function setIdViaje($idViaje)
    {
        $this->idViaje = $idViaje;

        return $this;
    }

    /**
     * Get idViaje
     *
     * @return int
     */
    public function getIdViaje()
    {
        return $this->idViaje;
    }
}

