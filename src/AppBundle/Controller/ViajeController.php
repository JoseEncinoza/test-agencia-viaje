<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\viaje;

class ViajeController extends Controller
{
	/**
     * @Route("/viaje", name="viaje_listar")
     * @Method({"GET"})
     */
    public function findAll()
    {	
    	// ----- Crear Manager de Doctrine -----
		$em = $this->getDoctrine()->getManager();
		// ----- Crear Query -----
		$query = $em->createQuery(
		    'SELECT c
		     FROM AppBundle:viaje c'
		);
    	// ----- Ejecutar Query -----
		$data = $query->getArrayResult();

        // ----- Buscar clientes asociados -----
        $length = sizeof($data);
        for ($i=0; $i < $length; $i++) {

            $query2 = $em->createQuery(
                'SELECT c
                 FROM AppBundle:cliente_viaje c
                 WHERE c.idViaje = :id'
            )
            ->setParameter('id', $data[$i]['id']);

            $data2 = $query2->getArrayResult();

            //----- Buscar datos del cliente-----
            $clientes = [];
            $max = sizeof($data2);

            for ($j=0; $j < $max; $j++) { 
                $query3 = $em->createQuery(
                    'SELECT c
                     FROM AppBundle:cliente c
                     WHERE c.id = :id'
                )
                ->setParameter('id', $data2[$j]['idCliente']);

                $data3 = $query3->getArrayResult();
                $clientes[$j] = $data3[0];
            }
        //----- Agregar clientes -----
        $data[$i]['clientes'] = $clientes;

        }
		// ----- Respuesta -----
		return new JsonResponse(array('estatus' => 200, 'data' => $data));
    }

    /**
     * @Route("/viaje/{id}", name="viaje_buscar")
     * @Method({"GET"})
     */
    public function findOne($id)
    {	
        // ----- Crear Manager de Doctrine -----
		$em = $this->getDoctrine()->getManager();
		// ----- Crear Query -----
		$query = $em->createQuery(
		    'SELECT c
		     FROM AppBundle:viaje c
		     WHERE c.id = :id'
		)
    	->setParameter('id', $id);
    	// ----- Ejecuta Query -----
		$data = $query->getArrayResult();

		if (!$data) {
			return new JsonResponse(array('estatus' => 404, 'mensaje' => 'viaje no existe'));
        }

        // ----- Buscar clientes asociados -----
            $query2 = $em->createQuery(
                'SELECT c
                 FROM AppBundle:cliente_viaje c
                 WHERE c.idViaje = :id'
            )
            ->setParameter('id', $data[0]['id']);

            $data2 = $query2->getArrayResult();

            //----- Buscar datos del cliente-----
            $clientes = [];
            $max = sizeof($data2);

            for ($i=0; $i < $max; $i++) { 
                $query3 = $em->createQuery(
                    'SELECT c
                     FROM AppBundle:cliente c
                     WHERE c.id = :id'
                )
                ->setParameter('id', $data2[$i]['idCliente']);

                $data3 = $query3->getArrayResult();
                $clientes[$i] = $data3[0];
            }
        //----- Agregar clientes -----
        $data[0]['clientes'] = $clientes;


		// ----- Respuesta -----
		return new JsonResponse(array('estatus' => 200, 'data' => $data[0]));
    }

    /**
     * @Route("/viaje", name="viaje_guardar")
     * @Method({"POST"})
     */
    public function create(Request $request)
    {
    	// ----- Crear Objeto -----
    	$viaje = new viaje();
    	// ----- Validar datos -----
    	if(!$request->request->get('codigo')) return new JsonResponse(array('estatus' => 500, 'mensaje' => 'campo codigo no puede estar vacio'));
        if(!$request->request->get('numero_plazas')) return new JsonResponse(array('estatus' => 500, 'mensaje' => 'campo numero_plazas no puede estar vacio'));
		if(!$request->request->get('destino')) return new JsonResponse(array('estatus' => 500, 'mensaje' => 'campo destino no puede estar vacio'));
		if(!$request->request->get('origen')) return new JsonResponse(array('estatus' => 500, 'mensaje' => 'campo origen no puede estar vacio'));
		if(!$request->request->get('precio')) return new JsonResponse(array('estatus' => 500, 'mensaje' => 'campo precio no puede estar vacio'));
    	// ----- Llenar Datos -----
        $viaje->setCodigo($request->request->get('codigo'));
        $viaje->setNumeroPlazas($request->request->get('numero_plazas'));
		$viaje->setDestino($request->request->get('destino'));
		$viaje->setOrigen($request->request->get('origen'));
		$viaje->setPrecio($request->request->get('precio'));

        // -----  Crear Manager de Doctrine ----- 
        $em = $this->getDoctrine()->getManager();
        // ----- Crear Registro ----- 
        $em->persist($viaje);
        // ----- Actualizar BD ----- 
        $em->flush();
        // ----- Respuesta ----- 
        return new JsonResponse(array('estatus' => 200, 'mensaje' => 'viaje guardado con exito'));
    }

    /**
     * @Route("/viaje/{id}", name="viaje_actualizar")
     * @Method({"PUT"})
     */
    public function update(Request $request, $id)
    {
        // ----- Crear Manager de Doctrine -----
	    $em = $this->getDoctrine()->getManager();
	    // ----- Buscar -----
        $viaje = $em->getRepository('AppBundle:viaje')->find($id);

        if (!$viaje) {
			return new JsonResponse(array('estatus' => 404, 'mensaje' => 'viaje no existe'));
        }
    	// ----- Llenar datos -----
    	if($request->request->get('codigo')) $viaje->setCodigo($request->request->get('codigo'));
        if($request->request->get('numero_plazas')) $viaje->setNumeroPlazas($request->request->get('numero_plazas'));
		if($request->request->get('destino')) $viaje->setDestino($request->request->get('destino'));
		if($request->request->get('origen')) $viaje->setOrigen($request->request->get('origen'));
		if($request->request->get('precio')) $viaje->setPrecio($request->request->get('precio'));
        // ----- Modificar Registro ----- 
        $em->persist($viaje);
        // ----- Actualizar BD ----- 
        $em->flush();
        // ----- Respuesta ----- 
        return new JsonResponse(array('estatus' => 200, 'mensaje' => 'viaje modificado con exito'));
    }

    /**
     * @Route("/viaje/{id}", name="viaje_eliminar")
     * @Method({"DELETE"})
     */
    public function delete(Request $request, $id)
    {
        // ----- Crear Manager de Doctrine -----
	    $em = $this->getDoctrine()->getManager();
	    // ----- Buscar -----
        $viaje = $em->getRepository('AppBundle:viaje')->find($id);

        if (!$viaje) {
			return new JsonResponse(array('estatus' => 404, 'mensaje' => 'viaje no existe'));
        }
        // ----- Eliminar Registro -----
        $em->remove($viaje);
        // ----- Actualizar BD -----
        $em->flush();
        // ----- Respuesta ----- 
        return new JsonResponse(array('estatus' => 200, 'mensaje' => 'viaje eliminado con exito'));
    }

}
