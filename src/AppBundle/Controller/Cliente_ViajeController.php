<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\cliente_viaje;

class Cliente_ViajeController extends Controller
{
	/**
     * @Route("/cliente_viaje", name="cliente_viaje_listar")
     * @Method({"GET"})
     */
    public function findAll()
    {	
    	// ----- Crear Manager de Doctrine -----
		$em = $this->getDoctrine()->getManager();
		// ----- Crear Query -----
		$query = $em->createQuery(
		    'SELECT c
		     FROM AppBundle:cliente_viaje c'
		);
    	// ----- Ejecutar Query -----
		$data = $query->getArrayResult();
		// ----- Respuesta -----
		return new JsonResponse(array('estatus' => 200, 'data' => $data));
    }

    /**
     * @Route("/cliente_viaje/{id}", name="cliente_viaje_buscar")
     * @Method({"GET"})
     */
    public function findOne($id)
    {	
        // ----- Crear Manager de Doctrine -----
		$em = $this->getDoctrine()->getManager();
		// ----- Crear Query -----
		$query = $em->createQuery(
		    'SELECT c
		     FROM AppBundle:cliente_viaje c
		     WHERE c.id = :id'
		)
    	->setParameter('id', $id);
    	// ----- Ejecuta Query -----
		$data = $query->getArrayResult();

		if (!$data) {
			return new JsonResponse(array('estatus' => 404, 'mensaje' => 'cliente_viaje no existe'));
        }
		// ----- Respuesta -----
		return new JsonResponse(array('estatus' => 200, 'data' => $data));
    }

    /**
     * @Route("/cliente_viaje", name="cliente_viaje_guardar")
     * @Method({"POST"})
     */
    public function create(Request $request)
    {
    	// ----- Crear Objeto -----
    	$cliente_viaje = new cliente_viaje();
    	// ----- Validar datos -----
    	if(!$request->request->get('id_cliente')) return new JsonResponse(array('estatus' => 500, 'mensaje' => 'campo id_cliente no puede estar vacio'));
        if(!$request->request->get('id_viaje')) return new JsonResponse(array('estatus' => 500, 'mensaje' => 'campo id_viaje no puede estar vacio'));
    	// ----- Llenar Datos -----
        $cliente_viaje->setIdCliente($request->request->get('id_cliente'));
        $cliente_viaje->setIdViaje($request->request->get('id_viaje'));
        // -----  Crear Manager de Doctrine ----- 
        $em = $this->getDoctrine()->getManager();
        // ----- Crear Registro ----- 
        $em->persist($cliente_viaje);
        // ----- Actualizar BD ----- 
        $em->flush();
        // ----- Respuesta ----- 
        return new JsonResponse(array('estatus' => 200, 'mensaje' => 'cliente_viaje guardado con exito'));
    }

    /**
     * @Route("/cliente_viaje/{id}", name="cliente_viaje_actualizar")
     * @Method({"PUT"})
     */
    public function update(Request $request, $id)
    {
        // ----- Crear Manager de Doctrine -----
	    $em = $this->getDoctrine()->getManager();
	    // ----- Buscar -----
        $cliente_viaje = $em->getRepository('AppBundle:cliente_viaje')->find($id);

        if (!$cliente_viaje) {
			return new JsonResponse(array('estatus' => 404, 'mensaje' => 'cliente_viaje no existe'));
        }
    	// ----- Llenar datos -----
    	if($request->request->get('id_cliente')) $cliente_viaje->setIdCliente($request->request->get('id_cliente'));
        if($request->request->get('id_viaje')) $cliente_viaje->setIdViaje($request->request->get('id_viaje'));
        // ----- Modificar Registro ----- 
        $em->persist($cliente_viaje);
        // ----- Actualizar BD ----- 
        $em->flush();
        // ----- Respuesta ----- 
        return new JsonResponse(array('estatus' => 200, 'mensaje' => 'cliente_viaje modificado con exito'));
    }

    /**
     * @Route("/cliente_viaje/{id}", name="cliente_viaje_eliminar")
     * @Method({"DELETE"})
     */
    public function delete(Request $request, $id)
    {
        // ----- Crear Manager de Doctrine -----
	    $em = $this->getDoctrine()->getManager();
	    // ----- Buscar -----
        $cliente_viaje = $em->getRepository('AppBundle:cliente_viaje')->find($id);

        if (!$cliente_viaje) {
			return new JsonResponse(array('estatus' => 404, 'mensaje' => 'cliente_viaje no existe'));
        }
        // ----- Eliminar Registro -----
        $em->remove($cliente_viaje);
        // ----- Actualizar BD -----
        $em->flush();
        // ----- Respuesta ----- 
        return new JsonResponse(array('estatus' => 200, 'mensaje' => 'cliente_viaje eliminado con exito'));
    }

}
