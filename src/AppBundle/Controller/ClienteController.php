<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\cliente;

class ClienteController extends Controller
{
	/**
     * @Route("/cliente", name="cliente_listar")
     * @Method({"GET"})
     */
    public function findAll()
    {	
    	// ----- Crear Manager de Doctrine -----
		$em = $this->getDoctrine()->getManager();
		// ----- Crear Query -----
		$query = $em->createQuery(
		    'SELECT c
		     FROM AppBundle:cliente c'
		);
    	// ----- Ejecutar Query -----
		$data = $query->getArrayResult();

        // ----- Buscar Viajes asociados -----
        $length = sizeof($data);
        for ($i=0; $i < $length; $i++) {

            $query2 = $em->createQuery(
                'SELECT c
                 FROM AppBundle:cliente_viaje c
                 WHERE c.idCliente = :id'
            )
            ->setParameter('id', $data[$i]['id']);

            $data2 = $query2->getArrayResult();

            //----- Buscar Datos del viaje-----
            $viajes = [];
            $max = sizeof($data2);

            for ($j=0; $j < $max; $j++) { 
                $query3 = $em->createQuery(
                    'SELECT c
                     FROM AppBundle:viaje c
                     WHERE c.id = :id'
                )
                ->setParameter('id', $data2[$j]['idViaje']);

                $data3 = $query3->getArrayResult();
                $viajes[$j] = $data3[0];
            }
            //----- Agregar Viajes -----
            $data[$i]['viajes'] = $viajes;

        }

		// ----- Respuesta -----
		return new JsonResponse(array('estatus' => 200, 'data' => $data));
    }

    /**
     * @Route("/cliente/{id}", name="cliente_buscar")
     * @Method({"GET"})
     */
    public function findOne($id)
    {	
        // ----- Crear Manager de Doctrine -----
		$em = $this->getDoctrine()->getManager();
		// ----- Crear Query -----
		$query = $em->createQuery(
		    'SELECT c
		     FROM AppBundle:cliente c
		     WHERE c.id = :id'
		)
    	->setParameter('id', $id);
    	// ----- Ejecuta Query -----
		$data = $query->getArrayResult();

		if (!$data) {
			return new JsonResponse(array('estatus' => 404, 'mensaje' => 'cliente no existe'));
        }

        // ----- Buscar Viajes Asociados -----
            $query2 = $em->createQuery(
                'SELECT c
                 FROM AppBundle:cliente_viaje c
                 WHERE c.idCliente = :id'
            )
            ->setParameter('id', $data[0]['id']);

            $data2 = $query2->getArrayResult();

            //----- Buscar Datos del viaje-----
            $viajes = [];
            $max = sizeof($data2);

            for ($i=0; $i < $max; $i++) { 
                $query3 = $em->createQuery(
                    'SELECT c
                     FROM AppBundle:viaje c
                     WHERE c.id = :id'
                )
                ->setParameter('id', $data2[$i]['idViaje']);

                $data3 = $query3->getArrayResult();
                $viajes[$i] = $data3[0];
            }
        //----- Agregar Viajes -----
        $data[0]['viajes'] = $viajes;

		// ----- Respuesta -----
		return new JsonResponse(array('estatus' => 200, 'data' => $data[0]));
    }

    /**
     * @Route("/cliente", name="cliente_guardar")
     * @Method({"POST"})
     */
    public function create(Request $request)
    {
    	// ----- Crear Objeto -----
    	$cliente = new cliente();
    	// ----- Validar datos -----
    	if(!$request->request->get('cedula')) return new JsonResponse(array('estatus' => 500, 'mensaje' => 'campo cedula no puede estar vacio'));
        if(!$request->request->get('nombre')) return new JsonResponse(array('estatus' => 500, 'mensaje' => 'campo nombre no puede estar vacio'));
		if(!$request->request->get('direccion')) return new JsonResponse(array('estatus' => 500, 'mensaje' => 'campo direccion no puede estar vacio'));
		if(!$request->request->get('telefono')) return new JsonResponse(array('estatus' => 500, 'mensaje' => 'campo telefono no puede estar vacio'));
    	// ----- Llenar Datos -----
        $cliente->setCedula($request->request->get('cedula'));
        $cliente->setNombre($request->request->get('nombre'));
		$cliente->setDireccion($request->request->get('direccion'));
		$cliente->setTelefono($request->request->get('telefono'));

        // -----  Crear Manager de Doctrine ----- 
        $em = $this->getDoctrine()->getManager();
        // ----- Crear Registro ----- 
        $em->persist($cliente);
        // ----- Actualizar BD ----- 
        $em->flush();
        // ----- Respuesta ----- 
        return new JsonResponse(array('estatus' => 200, 'mensaje' => 'cliente guardado con exito'));
    }

    /**
     * @Route("/cliente/{id}", name="cliente_actualizar")
     * @Method({"PUT"})
     */
    public function update(Request $request, $id)
    {
        // ----- Crear Manager de Doctrine -----
	    $em = $this->getDoctrine()->getManager();
	    // ----- Buscar -----
        $cliente = $em->getRepository('AppBundle:cliente')->find($id);

        if (!$cliente) {
			return new JsonResponse(array('estatus' => 404, 'mensaje' => 'cliente no existe'));
        }
    	// ----- Llenar datos -----
    	if($request->request->get('cedula')) $cliente->setCedula($request->request->get('cedula'));
        if($request->request->get('nombre')) $cliente->setNombre($request->request->get('nombre'));
		if($request->request->get('direccion')) $cliente->setDireccion($request->request->get('direccion'));
		if($request->request->get('telefono')) $cliente->setTelefono($request->request->get('telefono'));
        // ----- Modificar Registro ----- 
        $em->persist($cliente);
        // ----- Actualizar BD ----- 
        $em->flush();
        // ----- Respuesta ----- 
        return new JsonResponse(array('estatus' => 200, 'mensaje' => 'cliente modificado con exito'));
    }

    /**
     * @Route("/cliente/{id}", name="cliente_eliminar")
     * @Method({"DELETE"})
     */
    public function delete(Request $request, $id)
    {
        // ----- Crear Manager de Doctrine -----
	    $em = $this->getDoctrine()->getManager();
	    // ----- Buscar -----
        $cliente = $em->getRepository('AppBundle:cliente')->find($id);

        if (!$cliente) {
			return new JsonResponse(array('estatus' => 404, 'mensaje' => 'cliente no existe'));
        }
        // ----- Eliminar Registro -----
        $em->remove($cliente);
        // ----- Actualizar BD -----
        $em->flush();
        // ----- Respuesta ----- 
        return new JsonResponse(array('estatus' => 200, 'mensaje' => 'cliente eliminado con exito'));
    }

}
